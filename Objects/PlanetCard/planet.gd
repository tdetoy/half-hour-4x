extends Control

signal interacted

onready var planet = $Planet

var player_owner := -1
var points := 0
var food := 0
var ore := 0
var fuel := 0
var id := -1
var explored := false

var info setget set_info, get_private

func init(_args) -> void:
	pass

func _gui_input(event) -> void:
	if (!(event is InputEventMouseButton)):
		return
	if (event.pressed == true && event.button_index == BUTTON_LEFT):
		emit_signal("interacted")

func set_info(_info) -> void:
	info = _info

func get_private() -> void:
	pass

func set_player_color(color : Color) -> void:
	material.set_shader_param("player_color", color)

func set_planet_selectable(selectable : bool) -> void:
	planet.material.set_shader_param("active", selectable)

func set_explored() -> void:
	explored = true

	if (!info):
		print("setting a planet explored without any exploration info!")
		return
	
	var result = info.decode_data()
	
	if (result.size() <= 0):
		print("how did we get here")
	
	points = result[0]
	food = result[1]
	ore = result[2]
	fuel = result[3]
	
	planet.set_explored()

func minify(minified : bool) -> void:
	planet.visible = false
	planet = $SmallPlanet if minified else $Planet
	planet.visible = true
	rect_min_size = planet.rect_min_size
	if (explored && !planet.revealed):
		planet.set_explored()
