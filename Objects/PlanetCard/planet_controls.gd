extends PanelContainer

#these will not be updated to scene unique nodes as this script is used
#twice in a single scene to update two different sets of identically named
#nodes

#i could make it so that each set has unique names but
#
#eeeeehhhhh, why create two copies of the same script only to have different
#node references when exported nodepaths can do the trick

export(NodePath) var appearance_path
export(NodePath) var points_path
export(NodePath) var food_path
export(NodePath) var ore_path
export(NodePath) var fuel_path
export(NodePath) var unknown_1
export(NodePath) var unknown_2
export(NodePath) var unknown_3
export(NodePath) var unknown_4

onready var appearance = get_node(appearance_path)
onready var points_view = get_node(points_path)
onready var food_view = get_node(food_path)
onready var ore_view = get_node(ore_path)
onready var fuel_view = get_node(fuel_path)
onready var unknowns = [
	get_node(unknown_1),
	get_node(unknown_2),
	get_node(unknown_3),
	get_node(unknown_4)
]

onready var parent = get_parent()
var revealed = false

func _ready() -> void:
	material.set_shader_param("player_color", Color.black)

func _update_points_view() -> void:
	for point in points_view.get_children():
		point.visible = (((point.get_index() + 1) - parent.points) <= 0)

func _update_food_view() -> void:
	for food_child in food_view.get_children():
		food_child.visible = (((food_child.get_index() + 1) - parent.food) <= 0)

func _update_ore_view() -> void:
	for ore_child in ore_view.get_children():
		ore_child.visible = (((ore_child.get_index() + 1) - parent.ore) <= 0)

func _update_fuel_view() -> void:
	for fuel_child in fuel_view.get_children():
		fuel_child.visible = (((fuel_child.get_index() + 1) - parent.fuel) <= 0)

func set_explored() -> void:
	revealed = true
	for unknown in unknowns:
		unknown.visible = false
	
	_update_points_view()
	_update_food_view()
	_update_ore_view()
	_update_fuel_view()
	
	points_view.visible = true
	food_view.visible = true
	ore_view.visible = true
	fuel_view.visible = true

