extends PanelContainer

onready var stack_size: Label = $"%StackSize"

var num_cards := 0

func _ready():
	stack_size.text = str(num_cards)

func _on_stack_updated(value):
	num_cards = value
	stack_size.text = str(num_cards)
