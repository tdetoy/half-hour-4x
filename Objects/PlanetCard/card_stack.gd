extends Node

signal stack_updated(deck_size)
signal stack_empty

enum CARD_TYPES {
	FFP = 0,
	FFM = 1,
	FMM = 2,
	UMM = 3,
	UUP = 4
}

var deck = []

const BASE_AMOUNTS = [
	3,
	4,
	6,
	8,
	3
]

const ADD_AMOUNTS = [
	1,
	2,
	2,
	3,
	1
]


func init(args) -> void:
	pass

func _ready() -> void:
	if(get_tree().is_network_server()):
		build_deck(OnlineMatch.players.size())

#based on spreadsheet work, deck should be
#3 FFP
#4 FFM
#6 FMM
#8 UMM
#3 UUP
#with the values increasing per player by the ADD_AMOUNTS const
#so, determine num players, increase each item by ADD_AMOUNTS  * num players over 3
#and then add them to the deck
#index should align with CARD_TYPES enum
func build_deck(num_players) -> void:
	var new_deck = []

	for index in BASE_AMOUNTS.size():
		var num_to_add = BASE_AMOUNTS[index] + (ADD_AMOUNTS[index] * (num_players - 3))
		for _j in num_to_add:
			new_deck.append(index)
	
	rpc("report_deck", new_deck)

func draw_card() -> String:
	deck.shuffle()
	var draw = deck.pop_front()
	print(draw)
	deck.sort()
	rpc("report_deck", deck)
	var info = GameState.CardInfo.build_data(draw)
	return info


remotesync func report_deck(_deck) -> void:
	deck = _deck.duplicate()
	
	emit_signal("stack_updated", deck.size())
	
	if (deck.size() == 0):
		emit_signal("stack_empty")
	
	print(deck)


