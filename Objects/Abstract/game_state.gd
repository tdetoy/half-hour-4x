extends Node

signal planet_explored(planet_data, reveal_planet)
signal new_text(text, sender)
signal new_event(event, sender)
signal planet_settled(planet_id, player)


var local_player
var player_states = {}

class PlayerState:
	
	#[public_points, secret_points, ships, food, fuel, ore, fuel_differential, ore_differential]
	signal state_changed(stats)
	
	
	# planet id: planet node in scene
	var planets : Dictionary = {}
	var tableau : Node
	var is_local := false
	var public_points := 0
	var secret_points := 0
	var ships := 1
	var food := 1
	var fuel := 1
	var ore := 6
	var fuel_differential := 1
	var ore_differential := 1
	
	func new_planet(planet) -> void:
		planets[planet.id] = planet
		tableau.add_planet(planet)
		planet.minify(true)
		public_points += 1
		food -= 1
		if (is_local):
			planet.set_explored()
		if (planet.explored):
			food += planet.food
			secret_points += planet.points
			fuel_differential += planet.fuel
			ore_differential += planet.ore
		
		emit_signal("state_changed", [public_points, secret_points, ships, food, fuel, ore, fuel_differential, ore_differential])
	
	func ships_built(_ships, _ore) -> void:
		ships = _ships
		ore = _ore
		
		emit_signal("state_changed", [public_points, secret_points, ships, food, fuel, ore, fuel_differential, ore_differential])

class CardInfo:
	var data
	
	static func build_data(type) -> String:
		
		var _data = []
		
		#0 = FFP
		#1 = FFM
		#2 = FMM
		#3 = UMM
		#4 = UUP
		match type:
			
			0:
				_data.append(1)
				_data.append(2)
				_data.append(0)
				_data.append(0)
			1:
				_data.append(0)
				_data.append(2)
				_data.append(1)
				_data.append(0)
			2:
				_data.append(0)
				_data.append(1)
				_data.append(2)
				_data.append(0)
			3:
				_data.append(0)
				_data.append(0)
				_data.append(2)
				_data.append(1)
			4:
				_data.append(1)
				_data.append(0)
				_data.append(0)
				_data.append(2)
		
		var rotator = randi() % 4
		
		var rotated_data = []
		for i in _data.size():
			var index = wrapi(i + rotator, 0, _data.size())
			rotated_data.append(_data[index])
		
		rotated_data.push_front(rotator)
		
		return Marshalls.variant_to_base64(rotated_data)
		
	
	func decode_data():
		var decoded = Marshalls.base64_to_variant(data)
		var rotator = decoded.pop_front()
		var result = []
		for i in decoded.size():
			var index = wrapi(i - rotator, 0, decoded.size())
			result.append(decoded[index])
		
		return result

func _ready():
	pass

func setup_players() -> void:
	for player in OnlineMatch.players.keys():
		var new_player = PlayerState.new()
		var online_player = OnlineMatch.players[player]
		player_states[online_player.peer_id] = new_player
		if (online_player.peer_id == multiplayer.get_network_unique_id()):
			local_player = new_player
	
	print(player_states)
	print(local_player)

func send_planet_explore(result) -> void:
	rpc("receive_planet_explore", result)

remotesync func receive_planet_explore(info) -> void:
	var is_sender = multiplayer.get_rpc_sender_id() == multiplayer.get_network_unique_id()
	var data = CardInfo.new()
	data.data = info
	info = data
	emit_signal("planet_explored", info, is_sender)

func send_text(text, sender):
	rpc("receive_text", text, sender)

remotesync func receive_text(text, sender):
	emit_signal("new_text", text, sender)

func send_settled_planet(planet) -> void:
	rpc("receive_settled_planet", planet.id)

remotesync func receive_settled_planet(id) -> void:
	emit_signal("planet_settled", id, multiplayer.get_rpc_sender_id())

func _on_build(ships, ore) -> void:
	rpc("receive_ship_update", ships, ore)

func receive_ship_update(ships, ore) -> void:
	player_states[multiplayer.get_rpc_sender_id()].add_ships(ships, ore)
