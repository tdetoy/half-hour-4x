extends Control

enum STATES { NONE, DRAW, ASSESS, SETTLE, CONSTRUCT, TRADE, ATTACK }

var planet_card = preload("res://Objects/PlanetCard/Planet.tscn")
var tableau = preload("res://Objects/Player/Tableau/Tableau.tscn")
var build_ships_dialog = preload("res://UI/BuildShipsDialog.tscn")

onready var buttons: HBoxContainer = $"%Buttons"
onready var card_stack: Node = $"%CardStack"
onready var neutral_planets: ScrollContainer = $"%NeutralPlanetsView"
onready var card_stack_visual: PanelContainer = $"%PlanetPile"
onready var local_player_tableau: Control = $"%Tableau"
onready var networked_player_tableau_list: VBoxContainer = $"%PlayerList"

var can_draw := true
var can_assess := false
var can_settle := false
var can_construct := true
var can_trade := true
var can_attack := true
var id_tracker := 0
var state = STATES.NONE

func init(args) -> void:
	pass

func _gui_input(event):
	print(event)

func _ready() -> void:
	GameState.connect("planet_explored", self, "_on_planet_explored")
	GameState.connect("planet_settled", self, "_on_planet_settled")
	card_stack.connect("stack_updated", card_stack_visual, "_on_stack_updated")
	card_stack.connect("stack_empty", self, "_on_card_stack_empty")
	neutral_planets.connect("planets_updated", self, "_on_new_neutral")
	for button in buttons.button_list:
		button.connect("pressed", self, "_on_action_taken", [button.name])
	
	for player in GameState.player_states.keys():
		if (player == multiplayer.get_network_unique_id()):
			local_player_tableau.displayname.text = OnlineMatch.get_player_by_peer_id(player).username
			GameState.player_states[player].tableau = local_player_tableau
			GameState.player_states[player].connect("state_changed", local_player_tableau, "_on_state_change")
			GameState.player_states[player].connect("state_changed", buttons, "_on_state_change")
			continue
		var tableau_instance = tableau.instance()
		networked_player_tableau_list.add_child(tableau_instance)
		tableau_instance.displayname.text = OnlineMatch.get_player_by_peer_id(player).username
		GameState.player_states[player].tableau = tableau_instance
		GameState.player_states[player].connect("state_changed", tableau, "_on_state_change")

func _on_action_taken(name: String) -> void:
	match name:
		"Draw":
			if (!can_draw):
				return
			set_state(STATES.DRAW)
			draw()
		"Assess":
			if (!can_assess):
				return
			set_state(STATES.ASSESS)
			assess()
		"Settle":
			if (!can_settle):
				return
			set_state(STATES.SETTLE)
			settle()
		"Construct":
			if (!can_construct):
				return
			set_state(STATES.CONSTRUCT)
			construct()
		"Trade":
			if (!can_trade):
				return
			set_state(STATES.TRADE)
			pass
		"Attack":
			if (!can_attack):
				return
			set_state(STATES.ATTACK)
			pass

func set_state(_state) -> void:
	match state:
		STATES.ASSESS:
			neutral_planets.set_assess(false)
		STATES.SETTLE:
			neutral_planets.set_settleable(false)
	state = _state

func draw() -> void:
	var result = card_stack.draw_card()
	GameState.send_planet_explore(result)

func _on_card_stack_empty() -> void:
	can_draw = false

func assess() -> void:
	neutral_planets.set_assess(true)

func _on_planet_explored(result, explored) -> void:
	var new_planet = planet_card.instance()
	new_planet.info = result
	new_planet.id = id_tracker
	id_tracker += 1
	new_planet.connect("interacted", self, "planet_interacted", [new_planet])
	neutral_planets.add_planet(new_planet)
	if (explored):
		new_planet.set_explored()

func _on_new_neutral(amount) -> void:
	can_assess = amount > 0
	can_settle = amount > 0

func settle() -> void:
	if (GameState.local_player.ships < 1 || GameState.local_player.fuel < 1):
		set_state(STATES.NONE)
		return
	neutral_planets.set_settleable(true)

func construct() -> void:
	var dialog = build_ships_dialog.instance()
	add_child(dialog)
	dialog.populate(GameState.local_player.ore)
	dialog.connect("build", GameState, "_on_build")
	yield(dialog, "tree_exiting")
	set_state(STATES.NONE)

func planet_interacted(planet) -> void:
	match state:
		STATES.ASSESS:
			planet.set_explored()
		STATES.SETTLE:
			GameState.send_settled_planet(planet)
	
	set_state(STATES.NONE)

func _on_planet_settled(planet_id, player_id) -> void:
	var planet = neutral_planets.retrieve_planet_by_id(planet_id)
	
	assert(planet)
	
	planet.player_owner = player_id
	GameState.player_states[player_id].new_planet(planet)
