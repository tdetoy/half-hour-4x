extends ScrollContainer

signal planets_updated(num)

func add_planet(planet) -> void:
	$GridContainer.add_child(planet)
	emit_signal("planets_updated", $GridContainer.get_child_count())

func remove_planet(planet) -> void:
	$GridContainer.remove_child(planet)
	planet.disconnect("interacted", self, "planet_interacted")
	emit_signal("planets_updated", $GridContainer.get_child_count())

func set_assess(state : bool) -> void:
	for child in $GridContainer.get_children():
		if (child.explored || !state):
			child.set_planet_selectable(false)
		else:
			child.set_planet_selectable(true)

func set_settleable(state : bool) -> void:
	for child in $GridContainer.get_children():
		child.set_planet_selectable(true)

func retrieve_planet_by_id(planet_id) -> Node:
	for child in $GridContainer.get_children():
		if child.id == planet_id:
			$GridContainer.remove_child(child)
			return child
	return null
