extends HBoxContainer

var pressed_theme = preload("res://Assets/Temp/button_pressed.tres")

onready var draw: TextureButton = $"%Draw"
onready var assess: TextureButton = $"%Assess"
onready var settle: TextureButton = $"%Settle"
onready var construct: TextureButton = $"%Construct"
onready var trade: TextureButton = $"%Trade"
onready var attack: TextureButton = $"%Attack"
onready var ore_total: Label = $"%OreTotal"
onready var ore_differential: Label = $"%OreDifferential"
onready var fuel_total: Label = $"%FuelTotal"
onready var fuel_differential: Label = $"%FuelDifferential"
onready var food_differential: Label = $"%FoodDifferential"

onready var button_list := [
	draw,
	assess,
	settle,
	construct,
	trade,
	attack
]

func _ready() -> void:
	
	for button in button_list:
		button.connect("button_down", self, "_on_button_down", [button])
		button.connect("button_up", self, "_on_button_up", [button])

func _update_ore(total : int, differential : int) -> void:
	ore_total.text = str(total)
	ore_differential.text = ("(+" if differential >= 0 else "(-") + str(differential) + ")"

func _update_fuel(total : int, differential : int) -> void:
	fuel_total.text = str(total)
	fuel_differential.text = ("(+" if differential >= 0 else "(-") + str(differential) + ")"

func _update_food(differential : int) -> void:
	food_differential.text = ("(+" if differential >= 0 else "(-") + str(differential) + ")"

func _on_button_down(button) -> void:
	button.get_parent().theme = pressed_theme

func _on_button_up(button) -> void:
	button.get_parent().theme = null

func _on_state_change(status) -> void:
	_update_food(status[3])
	_update_fuel(status[4], status[6])
	_update_ore(status[5], status[7])
