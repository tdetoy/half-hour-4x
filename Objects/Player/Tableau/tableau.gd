extends Control


onready var appearance: PanelContainer = $"%Appearance"
onready var display_name: Label = $"%Name"
onready var points: Label = $"%Points"
onready var ships: Label = $"%ShipCount"
onready var card_slots: GridContainer = $"%CardSlots"

var username := ""

func set_player_color(color : Color) -> void:
	appearance.material.set_shader_param("player_color", color)

func add_planet(planet):
	card_slots.add_child(planet)

func _on_state_change(status) -> void:
	points.text = str(status[0] + status[1])
	ships.text = str(status[2])
