extends Control

var queued_player_scene = preload("res://UI/QueuedPlayer.tscn")

onready var player_list: VBoxContainer = $"%Players"
onready var chat_box: RichTextLabel = $"%ChatBox"
onready var chat_entry: LineEdit = $"%ChatEntry"

var ready_players = {}

var nakama_client : NakamaClient
var nakama_session : NakamaSession
var nakama_socket : NakamaSocket

func _ready() -> void:
	SceneHandler.set_current_scene(self)
	GameState.connect("new_text", self, "_update_chat")
	OnlineMatch.connect("error", self, "_on_OnlineMatch_error")
	OnlineMatch.connect("disconnected", self, "_on_OnlineMatch_disconnected")
	OnlineMatch.connect("match_created", self, "_on_OnlineMatch_match_created")
	OnlineMatch.connect("match_joined", self, "_on_OnlineMatch_match_joined")
	OnlineMatch.connect("matchmaker_matched", self, "_on_OnlineMatch_matchmaker_matched")
	OnlineMatch.connect("player_joined", self, "_on_OnlineMatch_player_joined")
	OnlineMatch.connect("player_left", self, "_on_OnlineMatch_player_left")
	OnlineMatch.connect("player_status_changed", self, "_on_OnlineMatch_player_status_changed")
	OnlineMatch.connect("match_ready", self, "_on_OnlineMatch_match_ready")
	OnlineMatch.connect("match_not_ready", self, "_on_OnlineMatch_match_not_ready")

func connect_to_nakama(username) -> void:
	$CenterContainer.visible = false
	nakama_client = Nakama.create_client("defaultkey", "localhost", 7350, "http",
	Nakama.DEFAULT_TIMEOUT, NakamaLogger.LOG_LEVEL.ERROR)
	
	var device_id = OS.get_unique_id()
	nakama_session = yield(nakama_client.authenticate_custom_async(device_id, username), "completed")
	
	if (nakama_session.is_exception()):
		print("Unable to connect to Nakama")
		get_tree().quit()
	
	nakama_socket = Nakama.create_socket_from(nakama_client)
	yield(nakama_socket.connect_async(nakama_session), "completed")
	
	print("Connected to Nakama")

	
	connect_to_an_online_match()

func connect_to_an_online_match() -> void:
	OnlineMatch.min_players = 3
	OnlineMatch.max_players = 6
	OnlineMatch.client_version = "dev"
	OnlineMatch.ice_servers = [{ "urls" : ["stun:stun.l.google.com:19302"] }]
	OnlineMatch.use_network_relay = OnlineMatch.NetworkRelay.AUTO
	
	OnlineMatch.start_matchmaking(nakama_socket)
	
	$MarginContainer.visible = true
	print("Joined matchmaking queue...")

func _on_OnlineMatch_error(message : String) -> void:
	print("ERROR: %s" % message)

func _on_OnlineMatch_disconnected() -> void:
	print("Disconnected from match")

func _on_OnlineMatch_match_created(match_id : String) -> void:
	print("Private match created: %s" % match_id)

func _on_OnlineMatch_match_joined(match_id : String) -> void:
	print("Joined private match: %s" % match_id)

func _on_OnlineMatch_matchmaker_matched(_players : Dictionary) -> void:
	_update_chat("Welcome to the chat!", "Server")
	
	var ready_node = queued_player_scene.instance()
	player_list.add_child(ready_node)
	ready_node.set_name(OnlineMatch.get_player_by_peer_id(multiplayer.get_network_unique_id()).username)
	
	ready_players[multiplayer.get_network_unique_id()] = [false, ready_node]

func _on_OnlineMatch_player_joined(player : OnlineMatch.Player) -> void:
	if (ready_players.keys().has(player.peer_id)):
		return
	
	var queued_player = queued_player_scene.instance()
	player_list.add_child(queued_player)
	queued_player.set_name(player.username)
	ready_players[player.peer_id] = [false, queued_player]
	
	chat_box.bbcode_text += player.username + " has joined the game" + "\n"

func _on_OnlineMatch_player_left(player : OnlineMatch.Player) -> void:
	if (!ready_players.has(player.peer_id)):
		return
	
	player_list.remove_child(ready_players[player.peer_id][1])
	ready_players.erase(player.peer_id)
	
	chat_box.bbcode_text += player.username + " has left the game" + "\n"

func _on_OnlineMatch_player_status_changed(player : OnlineMatch.Player, status) -> void:
	print("Player status changed: %s -> %s" % [player.username, status])
	
	if (player.peer_id != get_tree().get_network_unique_id() && status == OnlineMatch.PlayerStatus.CONNECTED):
		rpc_id(player.peer_id, "receive_ready", ready_players[multiplayer.get_network_unique_id()][0])

func _on_OnlineMatch_match_not_ready() -> void:
	print("Match not ready yet")

func _on_OnlineMatch_match_ready(players : Dictionary) -> void:
	print("Match ready! Players:")
	for player in players.values():
		print("%s" % player.username)

remotesync func start_game() -> void:
	GameState.setup_players()
	SceneHandler.change_scene("GameBoard")

remote func receive_ready(status : bool) -> void:
	if (ready_players.keys().has(multiplayer.get_rpc_sender_id())):
		return
	
	var ready_node = queued_player_scene.instance()
	player_list.add_child(ready_node)
	ready_node.set_name(OnlineMatch.get_player_by_peer_id(multiplayer.get_rpc_sender_id()).username)
	ready_node.set_ready(status)
	ready_players[multiplayer.get_rpc_sender_id()] = [status, ready_node]

func _on_Connect_pressed():
	var username = $CenterContainer/HBoxContainer/LineEdit.text
	
	if (username == ""):
		return
	
	connect_to_nakama(username)

func _on_Button_pressed():
	send_text()

func _on_LineEdit_text_entered(_text):
	send_text()

func send_text():
	if (chat_entry.text == ""):
		return
	
	var username = OnlineMatch.get_player_by_peer_id(get_tree().get_network_unique_id()).username
	GameState.send_text(chat_entry.text, username)
	chat_entry.text = ""

func _update_chat(message : String, username : String) -> void:
	chat_box.bbcode_text += username + ": " + message + "\n"

remotesync func report_ready(player) -> void:
	ready_players[player][0] = !ready_players[player][0]
	ready_players[player][1].set_ready(ready_players[player][0])
	
	var ready = true
	for player in ready_players.values():
		if (!player[0]):
			ready = false
	
	if (ready):
		rpc("start_game")

func _on_Ready_pressed():
	rpc("report_ready", multiplayer.get_network_unique_id())

func _on_Quit_pressed():
	get_tree().quit()
