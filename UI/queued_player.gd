extends HBoxContainer


func set_name(value : String) -> void:
	$Name.text = value

func set_ready(value : bool) -> void:
	$ReadyStatus.text = "Ready!" if value else "Not Ready"
