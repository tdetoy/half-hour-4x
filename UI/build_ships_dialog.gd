extends PopupDialog

signal build(ships, ore)

onready var ore_amount: Label = $"%OreAmount"
onready var spin_box: SpinBox = $"%SpinBox"
onready var build: Button = $"%Build"

var ore := 0

func populate(_ore) -> void:
	ore = _ore
	ore_amount.text = str(ore)
	#to allow for the tween effect
# warning-ignore:integer_division
	spin_box.max_value = (ore / 3) + 1
	popup_centered()

func _on_SpinBox_value_changed(value):
	build.disabled = int(spin_box.value) == 0
	if (int(value) == spin_box.max_value):
		$Tween.interpolate_property(ore_amount, "modulate", Color.red, Color.white, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		$Tween.start()
		spin_box.value -= 1
	ore_amount.text = str(ore - 3 * int(spin_box.value))


func _on_Build_pressed():
	var ships = int(spin_box.value)
	ore -= 3 * ships
	emit_signal("build", ships, ore)
	queue_free()


func _on_Cancel_pressed():
	queue_free()
