extends Control

onready var chat_box: RichTextLabel = $PanelContainer/VBoxContainer/PanelContainer/ChatBox
onready var chat_entry: LineEdit = $PanelContainer/VBoxContainer/HBoxContainer2/ChatEntry

var messages := {}

func _ready():
	GameState.connect("new_text", self, "_incoming_message")

func _incoming_message(text : String, sender : String) -> void:
	chat_box.bbcode_text += sender + ": " + text + "\n"

func _incoming_event(event : String, actor : String):
	chat_box.bbcode_text += actor + event + "\n"

func _on_Send_pressed() -> void:
	send_text()

func _on_LineEdit_text_entered(_text) -> void:
	send_text()

func send_text() -> void:
	if (chat_entry.text == ""):
		return
	
	var username = OnlineMatch.get_player_by_peer_id(get_tree().get_network_unique_id()).username
	GameState.send_text(chat_entry.text, username)
	chat_entry.text = ""
