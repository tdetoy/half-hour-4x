extends Node

const SCENES = {
	"GameBoard" : "res://Objects/Board/GameBoard.tscn"
}

var current_scene

func change_scene(scene : String, args : Array = []) -> void:
	get_tree().get_root().remove_child(current_scene)
	current_scene.queue_free()
	var new_scene = load(SCENES[scene]).instance()
	new_scene.init(args)
	get_tree().get_root().add_child(new_scene)
	current_scene = scene

func set_current_scene(node : Node) -> void:
	current_scene = node
